import React, { Component } from "react";
import Cart from "./Cart";
import { dataGioHang } from "./dataGioHang";
import ProductInfo from "./ProductInfo";
import ProductItem from "./ProductItem";
import ProductList from "./ProductList";

export default class BtGioHangDienThoai extends Component {
  state = {
    productList: dataGioHang,
    gioHang: [],
    isOpenModal: false,
    chiTietSp: [],
  };
  handleOpenModal = () => {
    this.setState({ isOpenModal: true });
  };
  handleCloseModal = () => {
    this.setState({ isOpenModal: false });
  };

  handleAddToCart = (sanPham) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == sanPham.maSP;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  handleDelProduct = (idSp) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == idSp;
    });
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleTangGiamSl = (idSp, giaTri) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == idSp;
    });
    if (index !== -1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };
  handleShowDetail = (sanPham) => {
    let cloneSanPham = [...this.state.chiTietSp];
    let index = cloneSanPham.findIndex((item) => {
      return item.maSP == sanPham.maSP;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham };
      cloneSanPham.splice(0, 1, newSanPham);
    }
    this.setState({ chiTietSp: cloneSanPham });
  };

  render() {
    return (
      <div>
        <h2 className="text-success pt-3">Bài tập giỏ hàng</h2>
        <Cart
          gioHang={this.state.gioHang}
          soLuong={this.state.gioHang.length}
          isOpenModal={this.state.isOpenModal}
          handleOpenModal={this.handleOpenModal}
          isCloseModal={this.handleCloseModal}
          handleXoaSp={this.handleDelProduct}
          handleTangGiamSl={this.handleTangGiamSl}
        />
        <ProductList
          handleThemSp={this.handleAddToCart}
          productList={this.state.productList}
          handleChiTietSp={this.handleShowDetail}
        />
        {this.state.chiTietSp.length > 0 && (
          <ProductInfo chiTietSp={this.state.chiTietSp} />
        )}
      </div>
    );
  }
}
