import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <div className="row pb-3">
          {this.props.productList.map((item, index) => {
            return (
              <ProductItem
                handleChiTietSp={this.props.handleChiTietSp}
                handleThemSp={this.props.handleThemSp}
                data={item}
                key={index}
              />
            );
          })}
        </div>
        <hr />
      </div>
    );
  }
}
