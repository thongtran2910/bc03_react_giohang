import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { hinhAnh, tenSP } = this.props.data;
    return (
      <div className="card col mx-2 py-3 text-left">
        <img src={hinhAnh} className="card-img-top h-75" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{tenSP}</h5>
          <a
            onClick={() => {
              this.props.handleChiTietSp(this.props.data);
            }}
            className="btn btn-success mr-2"
          >
            Xem chi tiết
          </a>
          <a
            onClick={() => {
              this.props.handleThemSp(this.props.data);
            }}
            className="btn btn-danger"
          >
            Thêm giỏ hàng
          </a>
        </div>
      </div>
    );
  }
}
