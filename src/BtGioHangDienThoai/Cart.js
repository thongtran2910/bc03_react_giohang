import React, { Component } from "react";
import { Modal, Button } from "antd";

export default class Cart extends Component {
  render() {
    return (
      <div className="container text-right">
        <a
          className="text-danger font-weight-bold"
          onClick={this.props.handleOpenModal}
        >
          Giỏ hàng: {this.props.soLuong}
        </a>
        <Modal
          title="Giỏ hàng"
          visible={this.props.isOpenModal}
          onOk={this.props.isCloseModal}
          onCancel={this.props.isCloseModal}
          width={1000}
        >
          <table className="table">
            <thead className="font-weight-bold">
              <td>Mã sản phẩm</td>
              <td>Hình ảnh</td>
              <td>Tên sản phẩm</td>
              <td>Số lượng</td>
              <td>Đơn giá</td>
              <td>Thành tiền</td>
              <td>Thao tác</td>
            </thead>
            <tbody>
              {this.props.gioHang.map((item) => {
                return (
                  <tr>
                    <td>{item.maSP}</td>
                    <td className="w-25">
                      <img className="w-25" src={item.hinhAnh} />
                    </td>
                    <td>{item.tenSP}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSl(item.maSP, 1);
                        }}
                        className="btn btn-light border-info py-0 px-1"
                      >
                        <i className="fa fa-plus text-secondary" />
                      </button>
                      <span className="mx-2">{item?.soLuong}</span>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSl(item.maSP, -1);
                        }}
                        className="btn btn-light border-info py-0 px-1"
                      >
                        <i className="fa fa-minus text-secondary" />
                      </button>
                    </td>
                    <td>{item.giaBan}</td>
                    <td>{item.giaBan * item?.soLuong}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleXoaSp(item.maSP);
                        }}
                        className="btn btn-danger"
                      >
                        Xóa
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Modal>
      </div>
    );
  }
}
