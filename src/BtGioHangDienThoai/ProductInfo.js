import React, { Component } from "react";

export default class ProductInfo extends Component {
  render() {
    return (
      <div className="container pt-3">
        {this.props.chiTietSp.map((item) => {
          return (
            <div className="row">
              <div className="col-4">
                <h3>{item.tenSP}</h3>
                <img className="img-fluid" src={item.hinhAnh} alt="" />
              </div>
              <div className="col-8 text-left">
                <table className="table">
                  <tr>
                    <td className="font-weight-bold" colSpan={2}>
                      Thông số kỹ thuật
                    </td>
                  </tr>
                  <tr>
                    <td>Màn hình</td>
                    <td>{item.manHinh}</td>
                  </tr>
                  <tr>
                    <td>Hệ điều hành</td>
                    <td>{item.heDieuHanh}</td>
                  </tr>
                  <tr>
                    <td>Camera trước</td>
                    <td>{item.cameraTruoc}</td>
                  </tr>
                  <tr>
                    <td>Camera sau</td>
                    <td>{item.cameraSau}</td>
                  </tr>
                  <tr>
                    <td>RAM</td>
                    <td>{item.ram}</td>
                  </tr>
                  <tr>
                    <td>ROM</td>
                    <td>{item.rom}</td>
                  </tr>
                </table>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}
