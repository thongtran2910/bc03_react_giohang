import logo from "./logo.svg";
import "./App.css";
import BtGioHangDienThoai from "./BtGioHangDienThoai/BtGioHangDienThoai";
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <BtGioHangDienThoai />
    </div>
  );
}

export default App;
